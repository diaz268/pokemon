import Api from './utils/api.js'
import Cards from './ui/cards.js'

const api = new Api()
const cards = new Cards()

const pokemonRender = async page => {
  const pokemons = await api.getAllPokemon(page)

  pokemons.map(item => {
    cards.addCard('pokemons', item)
  })
}

const loadPokemon = () => {
  const buttomLoad = document.getElementById('loadPokemon')
  buttomLoad.addEventListener('click', async () => {
    const pokemons = await api.nextPage()
    pokemons.map(item => {
      cards.addCard('pokemons', item)
    })
  })
}

pokemonRender()
loadPokemon()
