class Cards {
  addCard(id, pokemons) {
    const pokemonList = document.getElementById(id)
    const el = document.createElement('div')
    el.setAttribute('id', `pokemon_${pokemons.id}`)
    el.className =
      'card flex flex-col justify-center p-10 bg-gray-200 rounded-lg shadow-md hover:shadow-xl'
    el.innerHTML = `
    <div>
      <p class="text-2xl uppercase text-gray-900 font-bold">${pokemons.name}</p>
    </div>
    <div>
      <img src="${pokemons.image}"
        class="mt-6 h-48 object-cover object-center" />
    </div>
  `
    pokemonList.appendChild(el)
  }
}

export default Cards
