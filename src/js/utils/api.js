const url = 'https://pokeapi.co/api/v2/pokemon?'

class Api {
  constructor(limit = 6, page = 1) {
    this.limit = limit
    this.page = page
  }

  async getAllPokemon() {
    const limit = this.limit
    const offset = (this.page - 1) * this.limit

    try {
      const res = await fetch(url + new URLSearchParams({ limit, offset }))
      const data = await res.json()

      const arrPoke = await Promise.all(
        data.results.map(async item => {
          const pokemon = await fetch(item.url)
          const akl = await pokemon.json()
          return {
            id: akl.id,
            name: akl.name,
            image: akl.sprites.other.dream_world.front_default,
          }
        })
      )
      return arrPoke
    } catch (err) {
      console.error(err)
    }
  }

  nextPage() {
    this.page += 1
    return this.getAllPokemon()
  }
}

export default Api
