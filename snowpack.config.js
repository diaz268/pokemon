module.exports = {
  mount: {
    public: '/',
    src: '/assets',
  },
  plugins: ['@snowpack/plugin-postcss'],
  packageOptions: {
    /* ... */
  },
  devOptions: {
    /* ... */
  },
  buildOptions: {
    clean: true,
    out: 'dist',
  },
  optimize: {
    bundle: true,
    minify: true,
    target: 'es2018',
  },
}
